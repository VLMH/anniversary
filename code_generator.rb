SEC_FADE_IN = 6
SEC_FADE_OUT = 2
SEC_FADE_OUT_TEXT = 1
SEC_FADE_OUT_END = 1
IMAGE_DIR = '../images'
CSS_BROWSER_PREFIX = [
    '-webkit-',
    '-moz-',
    '-o-',
    '-ms-',
    ''
]

def total_sec; @images.count * SEC_FADE_IN; end
def fade_in_half_ptg; (fade_in_ptg.to_f / 2).floor; end
def fade_in_ptg; (100.to_f / @images.count).ceil; end
def fade_out_ptg; fade_in_ptg + (SEC_FADE_OUT.to_f / total_sec * 100).ceil; end
def fade_out_text_ptg; fade_in_ptg + (SEC_FADE_OUT_TEXT.to_f / total_sec * 100).ceil; end
def fade_out_end_ptg; fade_out_ptg + (SEC_FADE_OUT_END.to_f / total_sec * 100).ceil; end

def css_image_container
    <<-IMAGE_CONTAINER
.cb-slideshow li span {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0px;
    left: 0px;
    background-size: cover;
    background-position: 50% 50%;
    background-repeat: none;
    opacity: 0;
    z-index: 0;
    -webkit-backface-visibility: hidden;
    -webkit-animation: imageAnimation #{total_sec}s linear infinite 0s;
    -moz-animation: imageAnimation #{total_sec}s linear infinite 0s;
    -o-animation: imageAnimation #{total_sec}s linear infinite 0s;
    -ms-animation: imageAnimation #{total_sec}s linear infinite 0s;
    animation: imageAnimation #{total_sec}s linear infinite 0s;
}
    IMAGE_CONTAINER
end

def css_text_container
    <<-TEXT_CONTAINER
.cb-slideshow li div {
    z-index: 1000;
    position: absolute;
    bottom: 30px;
    left: 0px;
    width: 100%;
    text-align: center;
    opacity: 0;
    -webkit-animation: titleAnimation #{total_sec}s linear infinite 0s;
    -moz-animation: titleAnimation #{total_sec}s linear infinite 0s;
    -o-animation: titleAnimation #{total_sec}s linear infinite 0s;
    -ms-animation: titleAnimation #{total_sec}s linear infinite 0s;
    animation: titleAnimation #{total_sec}s linear infinite 0s;
}
    TEXT_CONTAINER
end

def css_elements(first_element, other_element)
    return '' if @images.empty?

    @images.each_with_index.inject('') do |output_css, path_with_index|
        path = File.join(IMAGE_DIR, File.basename(path_with_index[0]))
        index = path_with_index[1]
        output_css += index == 0 ? first_element.call(index, path) : other_element.call(index, path)
    end
end

def css_browsers(css_animation); CSS_BROWSER_PREFIX.map{|p| css_animation.call(p) }.join; end

def file_tokens(path); File.basename(path, '.jpg').split('_'); end
def name(path); file_tokens(path).first; end
def year(name); name.split('-').first; end
def message(path); file_tokens(path)[1..-1].unshift(year(name(path))).join('·'); end
def html_elements; @images.map{|p| "<li><span>#{name(p)}</span><div><h3>#{message(p)}</h3></div></li>"}.join("\n"); end

first_image = Proc.new { |index, path| ".cb-slideshow li:nth-child(#{index+1}) span { background-image: url(#{path}) }\n" }
other_image = Proc.new do |index, path|
    sec_delay = SEC_FADE_IN * index

    <<-CSS_IMAGE
.cb-slideshow li:nth-child(#{index+1}) span {
    background-image: url(#{path});
    -webkit-animation-delay: #{sec_delay}s;
    -moz-animation-delay: #{sec_delay}s;
    -o-animation-delay: #{sec_delay}s;
    -ms-animation-delay: #{sec_delay}s;
    animation-delay: #{sec_delay}s;
}
    CSS_IMAGE
end

first_text = Proc.new { '' }
other_text = Proc.new do |index|
    sec_delay = SEC_FADE_IN * index

    <<-CSS_TEXT
.cb-slideshow li:nth-child(#{index+1}) div {
    -webkit-animation-delay: #{sec_delay}s;
    -moz-animation-delay: #{sec_delay}s;
    -o-animation-delay: #{sec_delay}s;
    -ms-animation-delay: #{sec_delay}s;
    animation-delay: #{sec_delay}s;
}
    CSS_TEXT
end

image_animation = Proc.new do |prefix|
    <<-IMAGE_ANIMATION
@#{prefix}keyframes imageAnimation { 
    0% {
        opacity: 0;
        #{prefix}animation-timing-function: ease-in;
    }
    #{fade_in_half_ptg}% {
        opacity: 1;
        #{prefix}transform: scale(1.05);
        #{prefix}animation-timing-function: ease-out;
    }
    #{fade_in_ptg}% {
        opacity: 1;
        #{prefix}transform: scale(1.1);
    }
    #{fade_out_ptg}% {
        opacity: 0;
        #{prefix}transform: scale(1.1) translateY(-20%);
    }
    #{fade_out_end_ptg}% {
        opacity: 0;
        #{prefix}transform: scale(1.1) translateY(-100%);
    }
    100% { opacity: 0 }
}
    IMAGE_ANIMATION
end

text_animation = Proc.new do |prefix|
    <<-TEXT_ANIMATION
@#{prefix}keyframes titleAnimation { 
    0% {
        opacity: 0;
        #{prefix}transform: translateY(-300%);
    }
    #{fade_in_half_ptg}% {
        opacity: 1;
        #{prefix}transform: translateY(0%);
    }
    #{fade_in_ptg}% {
        opacity: 1;
        #{prefix}transform: translateY(0%);
    }
    #{fade_out_text_ptg}% {
        opacity: 0;
        #{prefix}transform: translateY(100%);
    }
    #{fade_out_end_ptg}% { opacity: 0 }
    100% { opacity: 0 }
}
    TEXT_ANIMATION
end

@images = Dir.glob("./images/*.jpg")
File.open('./output.css', 'w') do |f|
    f.write css_image_container
    f.write css_text_container
    f.write css_elements(first_image, other_image)
    f.write css_elements(first_text, other_text)
    f.write css_browsers(image_animation)
    f.write css_browsers(text_animation)
end
File.open('./output.html', 'w') {|f| f.write html_elements}
